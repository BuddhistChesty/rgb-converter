﻿using System;

namespace RgbConverter
{
    public static class Rgb
    {
        public static string GetHexRepresentation(int red, int green, int blue)
        {
            static string DecToHexa(int dec)
            {
                if (dec <= 0)
                {
                    return "00";
                }

                int hex = dec;
                string hexStr = string.Empty;
                if (hex < 16)
                {
                    hexStr = "0" + hexStr.Insert(0, Convert.ToChar(hex + 55).ToString());
                    return hexStr;
                }

                while (dec > 0)
                {
                    hex = dec % 16;

                    if (hex < 10)
                    {
                        hexStr = hexStr.Insert(0, Convert.ToChar(hex + 48).ToString());
                    }
                    else
                    {
                        hexStr = hexStr.Insert(0, Convert.ToChar(hex + 55).ToString());
                    }

                    dec /= 16;
                }

                return hexStr;
            }

            if (red > 255 || green > 255 || blue > 255)
            {
                return "FFFFFF";
            }

            string hexCode = string.Empty;
            hexCode += DecToHexa(red);
            hexCode += DecToHexa(green);
            hexCode += DecToHexa(blue);

            return hexCode;
        }
    }
}
